import os
import scipy.io as sp
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as ss

os.chdir(r'C:\Users\Ben\Documents\Research\12-01-2024_GlassyCarbon_MultiExp')  # Change according to computer using
data = sp.loadmat('01-11-2024_GlassyCarbon_CV_OCP_EIS_Map_Actual_PostCrash.mat')
traces = data.keys()
tracesList = list(traces)
testList = list(tracesList[:23])
mapList = list(tracesList[23:])

# What each square bracket of data[][][] returns
# data[ReturnsArrayOfTrace (Time, variable)][Returns a single time and variable][Returns variable]


# USER INPUT COME UP with PRINT STATEMENT ASKING FOR VARIABLES

mapSize = [10, 10]

experimentDictionary = {
    # Experiment Dictionary Contains ExperimentName: [NumberOfDuplicates, NumberOfVariables, PotentialRange, Time]
    'OCP': [2, 'EMon'],
    'CV': [4, 'EMon', 'IMon'],
    'LSV': [1, 'EMon', 'IMon']
}

LSVTimeinSeconds = 20
LSVRange = (-1,1)

listOfExperiments = ['OCP', 'CV', 'LSV']  # Write it in order of the experiment Including duplicates (i.e. [a, b, a])

totalVariables = 0
totalDuplicates = 0
for i in listOfExperiments:
    totalDuplicates += experimentDictionary[i][0]
    totalVariables += len(experimentDictionary[i]) - 1

numberOfSpots = mapSize[0] * mapSize[1]
tipDownCounter = remainingDuplicates = 0

# Maps Built as Number of Duplicates, Number of Spots, Number of Variables
# Creates 3D Array, 1 NumberOfSpots x NumberOfVariables array per duplicate.
# To reference [DuplicateYouWantToAccess, Spot, Variable]


if 'CV' in listOfExperiments:
    CVMap = np.zeros((experimentDictionary['CV'][0], numberOfSpots, len(experimentDictionary['CV'])), dtype=list)

if 'EIS' in listOfExperiments:
    EISMap = np.zeros((experimentDictionary['EIS'][0], numberOfSpots, len(experimentDictionary['ESI'])), dtype=list)

if 'LSV' in listOfExperiments:
    LSVMap = np.zeros((experimentDictionary['LSV'][0], numberOfSpots, len(experimentDictionary['LSV'])), dtype=list)

if 'OCP' in listOfExperiments:
    OCPMap = np.zeros((experimentDictionary['OCP'][0], numberOfSpots, len(experimentDictionary['OCP'])), dtype=list)

# m = np.array([14, 19, 22, 27, 31, 35, 38, 43, 46])
# n = m-13
#
# for i in range(len(n)):
#     print(str(mapList[n[i]]) + '\t' + str(data[mapList[n[i]]].shape))
#     print(data[mapList[n[i]]][:500])
#     print()


for i in range(len(mapList)):

    if str(data[mapList[i]][0][1]) == 'nan':
        # Finding all new spots (i.e. where we are able to tip down and run new set of expts)
        tipDownCounter += 1
        remainingDuplicates = totalDuplicates
        print(mapList[i] + ' is TipDown')

    elif i < len(mapList) - 1:

        if mapList[i][:-1] == mapList[i + 1][:-1]:
            # Finding all experiments with 2 variables (variables split between 2 different traces)
            # This code works by identifying the first of the two variables
            # In the case for CV and LSV this is IMon

            # Identifying CV
            # Cyclic behaviour shows rounded starting potential = rounded end potential
            if round(data[mapList[i + 1]][0][1], 3) == round(data[mapList[i + 1]][-1][1], 3):
                print(mapList[i] + ' is CV')
                print(mapList[i + 1] + ' is CV')

            # Identifying LSV
            # Should scan from one range to another. Checks if slope of potential = finalPot - initialPot / TimeOfLSV(s)
            # Will have to modify if scans from
            if -0.005 < (data[mapList[i + 1]][-1][1] + data[mapList[i + 1]][0][1]) < 0.005:
                print(mapList[i] + ' is LSV')
                print(mapList[i + 1] + ' is LSV')


        # Identifying OCP checks if slope of potential is close to 0.
        elif mapList[i][:-1] != mapList[i - 1][:-1] and mapList[i][-1] == '1':
            print(mapList[i] + ' is OCP')

# for i in range(len(mapList)):
#
#     if (i % (totalVariables*totalDuplicates + 1)) == 0 or i == 0:
#         tipDownCounter += 1
#         remainingDuplicates = totalDuplicates

# CV if the inital and final datapoints are similar
# elif int(data[mapList[i]][1] == )
# elif int(mapList[i][-1]) == 1 and remainingDuplicates > 0:
#     addToMap = []
#     for x in range(len(data[mapList[i]])):
#         addToMap.append(data[mapList[i]][x][1])
#
#     CVMap[(experimentDictionary['CV'][0] - remainingDuplicates), (tipDownCounter - 1), 1] = addToMap
#
# elif int(mapList[i][-1]) == 2 and remainingDuplicates > 0:
#     addToMap = []
#
#     for x in range(len(data[mapList[i]])):
#         addToMap.append(data[mapList[i]][x][1])
#
#     CVMap[(experimentDictionary['CV'][0] - remainingDuplicates), (tipDownCounter - 1), 0] = addToMap
#     remainingDuplicates -= 1

# print(str(data[mapList[i]].shape) + "\t" + str(i) + "\t" + mapList[i])

# OCP if last few

# LSV if first and last points are far from one another

# for x in range(numberOfSpots):
#     for y in range(totalDuplicates):
#
#         smoothCurrent = ss.savgol_filter(CVMap[y, x, 1], 20, 10)
#         # smoothPotential = ss.savgol_filter(CVMap[y, x, 0], 20, 3)
#
#         plt.plot(CVMap[y, x, 0], smoothCurrent, label="Replicate " + str(y+1), linewidth=1, markersize=1)
#
#     plt.title("Cyclic Voltammogram Spot " + str(x+1))
#     plt.legend()
#     plt.ylim(-0.6e-9, 0.6e-9)
#     plt.xlabel('Potential (V vd Ag/AgCl)')
#     plt.ylabel('Current (A)')
#
    # plt.show()
